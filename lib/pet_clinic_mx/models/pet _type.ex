defmodule PetClinicMx.Models.PetType do
  @moduledoc """
    model for a type of pet types
  """
  use Ecto.Schema
  import Ecto.Changeset

  schema "pet_types" do
    field :name, :string

    timestamps()
  end

  @doc false
  def changeset(pet, attrs) do
    pet
    |> cast(attrs, [:name])
    |> validate_required([:name])
  end
end
