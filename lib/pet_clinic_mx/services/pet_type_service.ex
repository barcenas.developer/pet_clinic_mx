defmodule PetClinicMx.Services.PetTypeService do
  @moduledoc """
  The PetTypeService
  """
  import Ecto.Query, warn: false
  alias PetClinicMx.Repo

  alias PetClinicMx.Models.PetType

  def create_pet_type(attrs \\ %{}) do
    %PetType{}
    |> PetType.changeset(attrs)
    |> Repo.insert()
  end

  def list_pet_types() do
    Repo.all(PetType)
  end

  def get_pet_types!(id), do: Repo.get!(PetType, id)
end
