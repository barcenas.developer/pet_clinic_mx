defmodule PetClinicMx.AppointmentServiceTest do
  use PetClinicMx.DataCase
  alias PetClinicMx.Models.Appointment
  alias PetClinicMx.Services.AppointmentService
  import PetClinicMx.AppointmentServiceFixtures
  import PetClinicMx.OwnerServiceFixtures
  import PetClinicMx.HealthExpertServiceFixtures
  import PetClinicMx.PetTypeServiceFixtures
  import PetClinicMx.PetClinicServiceFixtures

  describe "appointments" do
    setup [
      :owner_fixture,
      :health_expert_fixture,
      :pet_type_fixture,
      :pet_fixture,
      :appointment_fixture
    ]

    @invalid_attrs %{
      healt_expert_id: nil,
      pet_id: nil,
      datetime: nil
    }

    test "list_appointment/0 returns all appointments", %{
      appointment_0: appointment_1,
      appointment_1: appointment_2
    } do
      assert AppointmentService.list_appointment() == [appointment_1, appointment_2]
    end

    test "appointment!/1 returns the appointment with given id", %{appointment_0: appointment} do
      assert AppointmentService.get_appointment!(appointment.id) == appointment
    end

    test "create_appointment/1 with valid data creates a appointment" do
      valid_attrs = %{
        healt_expert_id: 2,
        pet_id: 2,
        datetime: ~U[2022-05-10 04:21:10Z]
      }

      assert {:ok, %Appointment{}} = AppointmentService.create_appointment(valid_attrs)
    end

    # 
    test "create_appointment/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = AppointmentService.create_appointment(@invalid_attrs)
    end

    test "update_appointment/2 with valid data updates the appointment", %{
      appointment_0: appointment
    } do
      update_attrs = %{
        healt_expert_id: 10_527,
        pet_id: 5011,
        datetime: ~N[2022-04-07 22:56:24]
      }

      assert {:ok, %Appointment{}} =
               AppointmentService.update_appointment(appointment, update_attrs)
    end

    test "update_appointment/2 with invalid data returns error changeset", %{
      appointment_1: appointment
    } do
      assert {:error, %Ecto.Changeset{}} =
               AppointmentService.update_appointment(appointment, @invalid_attrs)

      assert appointment == AppointmentService.get_appointment!(appointment.id)
    end

    test "delete_appointment/1 deletes the expert", %{appointment_0: appointment} do
      assert {:ok, %Appointment{}} = AppointmentService.delete_appointment(appointment)

      assert_raise Ecto.NoResultsError, fn ->
        AppointmentService.get_appointment!(appointment.id)
      end
    end

    test "change_appointment/1 returns a appointment changeset", %{appointment_0: appointment} do
      assert %Ecto.Changeset{} = AppointmentService.change_appointment(appointment)
    end
  end
end
