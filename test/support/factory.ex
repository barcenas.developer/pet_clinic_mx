defmodule PetClinicMx.Factory do
  @moduledoc false
  use ExMachina.Ecto, repo: PetClinicMx.Repo

  def pet_factory do
    %PetClinicMx.Models.Pet{
      age: 42,
      name: "some name",
      sex: "female"
    }
  end

  def expert_factory do
    %PetClinicMx.Models.HealthExpert{
      age: 42,
      email: "some email",
      name: "some name",
      sex: "female"
    }
  end

  def owner_factory do
    %PetClinicMx.Models.Owner{
      age: 42,
      email: "some email",
      name: "some name",
      phone_num: "some phone_num"
    }
  end

  def pet_type_factory do
    %PetClinicMx.Models.PetType{
      name: "some name"
    }
  end

  def expert_schedule_factory do
    %PetClinicMx.Models.ExpertSchedule{
      healt_expert_id: 2,
      start_date: ~D[2022-05-01],
      ending_date: ~D[2022-05-11],
      start_hour: ~T[18:00:00],
      ending_hour: ~T[18:00:00]
    }
  end

  def appointment_factory do
    %PetClinicMx.Models.Appointment{
      healt_expert_id: 2,
      pet_id: 2,
      datetime: ~N[2022-04-07 22:57:27]
    }
  end
end
