defmodule PetClinicMx.AppointmentServiceFixtures do
  @moduledoc """
  The module for AppointmentServiceFixtures
  """
  alias PetClinicMx.PetClinicServiceFixtures
  alias PetClinicMx.HealthExpertServiceFixtures

  @doc """
  Generate a appointment.
  """

  # def appointment_fixture(attrs \\ %{}) do
  #   pet = PetClinicServiceFixtures.pet_fixture()
  #   expert = HealthExpertServiceFixtures.health_expert_fixture()
  # 
  #   {:ok, appointment} =
  #     attrs
  #     |> Enum.into(%{
  #       datetime: ~U[2022-05-10 04:21:10Z],
  #       pet_id: pet.id,
  #       healt_expert_id: expert.id
  #     })
  #     |> PetClinicMx.Services.AppointmentService.create_appointment()
  # 
  #   appointment
  # end

  import PetClinicMx.Factory

  def appointment_fixture(attrs \\ %{}) do
    pet = attrs.pet_0
    expert = attrs.expert_0

    %{
      appointment_0:
        insert(:appointment,
          healt_expert_id: expert.id,
          pet_id: pet.id,
          datetime: ~U[2022-05-10 04:21:10Z]
        ),
      appointment_1:
        insert(:appointment,
          healt_expert_id: expert.id,
          pet_id: pet.id,
          datetime: ~U[2022-05-10 04:21:10Z]
        )
    }
  end
end
