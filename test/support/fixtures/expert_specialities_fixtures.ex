defmodule PetClinicMx.ExpertSpecialitiesServiceFixtures do
  @moduledoc """
  The module for ExpertSpecialitiesServiceFixtures
  """
  alias PetClinicMx.HealthExpertServiceFixtures

  @doc """
  Generate a expert_schedule.
  """
  def expert_schedule_fixture(attrs \\ %{}) do
    expert = HealthExpertServiceFixtures.health_expert_fixture()

    {:ok, expert_schedule} =
      attrs
      |> Enum.into(%{
        start_date: ~D[2022-01-01],
        end_date: ~D[2022-01-01],
        end_hour: ~T[14:00:00],
        start_hour: ~T[14:00:00],
        week_num: 42,
        expert_id: expert.id
      })
      |> PetClinicMx.Services.ExpertScheduleService.create_schedule()

    expert_schedule
  end
end
