defmodule PetClinicMx.HealthExpertServiceFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `PetClinicMx.Services.PetHealthExpert` context.
  """

  alias PetClinicMx.PetClinicServiceFixtures
  # alias PetClinicMx.OwnerServiceFixtures
  alias PetClinicMx.HealthExpertServiceFixtures

  @doc """
  Generate a healt.
  """

  # def health_expert_fixture(attrs \\ %{}) do
  #   pet = PetClinicServiceFixtures.pet_fixture()
  #   # owner = OwnerServiceFixtures.owner_fixture()
  #   prefered_expert = HealthExpertServiceFixtures.health_expert_fixture()
  # 
  #   {:ok, healt} =
  #     attrs
  #     |> Enum.into(%{
  #       age: 42,
  #       email: "some email",
  #       name: "some name",
  #       sex: :male,
  #       patients_id: pet.id,
  #       preferred_expert_id: prefered_expert.id,
  #       appointments_id: 1,
  #       schedule_id: 1
  #     })
  #     |> PetClinicMx.Services.HealthExpertService.create_health_expert()
  # 
  #   healt
  # end

  import PetClinicMx.Factory

  def health_expert_fixture(attrs \\ %{}) do
    %{
      expert_0:
        insert(:expert,
          age: 42,
          email: "some email",
          name: "some name",
          sex: "female"
        ),
      expert_1:
        insert(:expert,
          age: 43,
          email: "some update email",
          name: "some update name",
          sex: "female"
        )
    }
  end
end
