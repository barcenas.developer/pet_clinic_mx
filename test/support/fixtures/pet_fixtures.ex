defmodule PetClinicMx.PetClinicServiceFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `PetClinicMx.Services.PetService`.
  """

  # Generate a pet.
  # """
  # def pet_fixture(attrs \\ %{}) do
  #  type = PetTypeServiceFixtures.pet_type_fixture()
  #  owner = OwnerServiceFixtures.owner_fixture()
  #  prefered_expert = HealthExpertServiceFixtures.health_expert_fixture()
  #  
  #  {:ok, pet} =
  #    attrs
  #    |> Enum.into(%{
  #      age: 41,
  #      name: "some name",
  #      sex: :male,
  #      type_id: type.id,
  #      owner_id: owner.id,
  #      preferred_expert_id: prefered_expert.id
  #    })
  #    |> PetClinicMx.Services.PetService.create_pet()
  #
  #  pet
  # end

  import PetClinicMx.Factory

  def pet_fixture(attrs \\ %{}) do
    owner = attrs.owner_0
    expert = attrs.expert_0
    pet_type = attrs.pet_type_0

    %{
      pet_0:
        insert(:pet,
          age: 42,
          name: "some name",
          sex: "female",
          type_id: pet_type.id,
          owner_id: owner.id,
          preferred_expert_id: expert.id
        ),
      pet_1:
        insert(:pet,
          age: 43,
          name: "some update name",
          sex: "female",
          type_id: pet_type.id,
          owner_id: owner.id,
          preferred_expert_id: expert.id
        )
    }
  end
end
