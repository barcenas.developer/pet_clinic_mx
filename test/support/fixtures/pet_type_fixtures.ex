defmodule PetClinicMx.PetTypeServiceFixtures do
  @moduledoc """
      The module for PetTypeServiceFixtures
  """

  # alias PetClinicMx.Services.PetTypeService

  import PetClinicMx.Factory

  @doc """
  Creates a pet_type
  """

  # def pet_type_fixture(attrs \\ %{}) do
  #  {:ok, pet_type} =
  #    attrs
  #    |> Enum.into(%{
  #      name: "doki"
  #    })
  #    |> PetTypeService.create_pet_type()
  #
  #  pet_type
  # end

  def pet_type_fixture(attrs \\ %{}) do
    %{
      pet_type_0:
        insert(:pet_type,
          name: "some name"
        ),
      pet_type_1:
        insert(:pet_type,
          name: "some update name"
        )
    }
  end
end
